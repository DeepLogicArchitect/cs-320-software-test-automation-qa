/*********************************************************
 *
 * Class Name: 	ContactTest
 * Author: 		Jason Gross
 * Date: 		9/19/2021
 * Purpose: 	Junit tests for Contact functionality
 * 
 *********************************************************/

package tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import main.*;

public class ContactTest {

	@Test
	public void NewContactGoodParams() {
		
		// Create new contact with valid parameters
		Contact c1 = new Contact("Moe", "Howard", "7175551234","123 This Street");
		
		// Test that parameters have been successfully set
		assertEquals("Moe", c1.getFirstName());
		assertEquals("Howard", c1.getLastName());
		assertEquals("7175551234", c1.getPhone());
		assertEquals("123 This Street", c1.getAddress());
	}
	
	@Test
	public void NewContactBadParamsLongFirstName() {
		
		// Test passing first name longer than 10 characters throws invalid first name exception
		try {
			new Contact("Bernardiney", "Howard", "7175551234","123 This Street");
        } catch (IllegalArgumentException thrown) {
        	assertTrue(thrown.getMessage().equals("Invalid first name"));
        }
	}
	
	@Test
	public void NewContactBadParamsNullFirstName() {
		
		// Test passing null first name throws invalid first name exception
		try {
			new Contact(null, "Howard", "7175551234","123 This Street");
        } catch (IllegalArgumentException thrown) {
        	assertTrue(thrown.getMessage().equals("Invalid first name"));
        }
	}
	
	@Test
	public void NewContactBadParamsLongLastName() {
		
		// Test passing last name longer than 10 characters throws invalid last name exception
		try {
			new Contact("Moe", "Bernardiney", "7175551234","123 This Street");
	    } catch (IllegalArgumentException thrown) {
	    	assertTrue(thrown.getMessage().equals("Invalid last name"));
	    }
	}
	
	@Test
	public void NewContactBadParamsNullLastName() {
		
		// Test passing null last name throws invalid last name exception
		try {
			new Contact("Moe", null, "7175551234","123 This Street");
        } catch (IllegalArgumentException thrown) {
        	assertTrue(thrown.getMessage().equals("Invalid last name"));
        }
	}
	
	@Test
	public void NewContactBadParamsLongPhoneNumber() {
		
		// Test passing phone number longer than 10 digits throws invalid phone exception
		try {
			new Contact("Moe", "Howard", "71755512345","123 This Street");
        } catch (IllegalArgumentException thrown) {
        	assertTrue(thrown.getMessage().equals("Invalid phone number"));
        }
	}
	
	@Test
	public void NewContactBadParamsShortPhoneNumber() {
		
		// Test passing phone number shorter than 10 digits throws invalid phone exception
		try {
			new Contact("Moe", "Howard", "5551234","123 This Street");
        } catch (IllegalArgumentException thrown) {
        	assertTrue(thrown.getMessage().equals("Invalid phone number"));
        }
	}
	
	@Test
	public void NewContactBadParamsNullPhoneNumber() {
		
		// Test passing null phone number throws invalid phone exception
		try {
			new Contact("Moe", "Howard", "", "123 This Street");
        } catch (IllegalArgumentException thrown) {
        	assertTrue(thrown.getMessage().equals("Invalid phone number"));
        }
	}
	
	@Test
	public void NewContactBadParamsNonNumericPhoneNumber() {
		
		// Test passing non-numeric phone number throws invalid phone exception
		try {
			new Contact("Moe", "Howard", "717555123A","123 This Street");
        } catch (IllegalArgumentException thrown) {
        	assertTrue(thrown.getMessage().equals("Invalid phone number"));
        }
	}
	
	@Test
	public void NewContactBadParamsLongAddress() {
		
		// Test passing address longer than 30 characters throws invalid address exception
		try {
			new Contact("Moe", "Howard", "7175551234","1234 Supercalifragilisticexpialidocious Lane");
        } catch (IllegalArgumentException thrown) {
        	assertTrue(thrown.getMessage().equals("Invalid address"));
        }
	}
	
	@Test
	public void NewContactBadParamsNullAddress() {
		
		// Test passing null address throws invalid address exception
		try {
			new Contact("Moe", "Howard", "7175551234","");
        } catch (IllegalArgumentException thrown) {
        	assertTrue(thrown.getMessage().equals("Invalid address"));
        }
	}
}
