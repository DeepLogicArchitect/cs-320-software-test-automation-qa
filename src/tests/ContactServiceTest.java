/*********************************************************
 *
 * Class Name: 	ContactServiceTest
 * Author: 		Jason Gross
 * Date: 		9/19/2021
 * Purpose: 	Junit tests for ContactService functionality
 * 
 *********************************************************/

package tests;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import main.*;

public class ContactServiceTest {

	@Test
	public void TestAddContact() {
		
		// Create new contact service
		ContactService cs = new ContactService();
		
		// Create new contact
		Contact c1 = new Contact("Moe", "Howard", "7175551234","123 This Street");
		
		// Test adding valid contact to service expecting success
		assertEquals(true, cs.addContact(c1));		
	}
	
	@Test
	public void TestAddBadContact() {
		
		// Create new contact service
		ContactService cs = new ContactService();
		
		// Create new contact
		Contact c1 = new Contact("Moe", "Howard", "7175551234","123 This Street");
		
		// Add contact to service
		cs.addContact(c1);
		
		// Create second new contact with identical parameters
		Contact c2 = new Contact("Moe", "Howard", "7175551234","123 This Street");
		
		// Test adding second contact to service expecting failure for duplicate parameters
		assertEquals(false, cs.addContact(c2));	
	}
	
	@Test
	public void TestRemoveKnownContact() {
		
		// Create new contact service
		ContactService cs = new ContactService();
		
		// Create new contact
		Contact c1 = new Contact("Moe", "Howard", "7175551234","123 This Street");
		
		// Add contact to service
		cs.addContact(c1);
		
		// Test removing known contact from service expecting success
		assertEquals(true, cs.removeContact(c1.getID()));		
	}
	
	@Test
	public void TestRemoveUnknownContact() {
		
		// Create new contact service
		ContactService cs = new ContactService();
		
		// Create new contact
		Contact c1 = new Contact("Moe", "Howard", "7175551234","123 This Street");
		
		// Create another new contact
		Contact c2 = new Contact("Larry", "Fine", "7175555678","123 That Street");
		
		// Add first contact to service but not the second
		cs.addContact(c1);
		
		// Test removing unknown contact from service expecting failure
		assertEquals(false, cs.removeContact(c2.getID()));		
	}
	
	@Test
	public void TestUpdateGoodContact() {
		
		// Create new contact service
		ContactService cs = new ContactService();
		
		// Create new contact
		Contact c1 = new Contact("Moe", "Howard", "7175551234","123 This Street");
		
		// Add contact to service
		cs.addContact(c1);
				
		// Test updating contact with new parameters expecting success
		assertEquals(true, cs.updateContact(c1.getID(),"Larry", "Fine", "7175555678","234 Different Street"));
		
		// Test that all parameters have truly updated
		assertEquals("Larry", c1.getFirstName());
		assertEquals("Fine", c1.getLastName());		
		assertEquals("7175555678", c1.getPhone());
		assertEquals("234 Different Street", c1.getAddress());		
	}
	
	@Test
	public void TestUpdateBadContact() {
		
		// Create new contact service
		ContactService cs = new ContactService();
		
		// Create new contact
		Contact c1 = new Contact("Moe", "Howard", "7175551234","123 This Street");
		
		// Add contact to service
		cs.addContact(c1);
				
		// Test updating contact with duplicate parameters expecting failure
		assertEquals(false, cs.updateContact(c1.getID(),"Moe", "Howard", "7175551234","123 This Street"));
		
		// Test updating contact with a different errant parameter each time expecting all failures
		assertEquals(false, cs.updateContact(c1.getID(),"Bernardiney", "Howard", "7175555678","234 Different Street"));	
		assertEquals(false, cs.updateContact(c1.getID(),"Moe", "Bernardiney", "7175555678","234 Different Street"));	
		assertEquals(false, cs.updateContact(c1.getID(),"Moe", "Howard", "717555567A","234 Different Street"));	
		assertEquals(false, cs.updateContact(c1.getID(),"Moe", "Howard", "7175555678","1234 Supercalifragilisticexpialidocious Lane"));
		assertEquals(false, cs.updateContact(c1.getID(),"", "", "",""));
	}	
}