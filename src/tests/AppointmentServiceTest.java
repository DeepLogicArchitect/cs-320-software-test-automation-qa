/*********************************************************
*
* Class Name: 	AppointmentServiceTest
* Author: 		Jason Gross
* Date: 		10/3/2021
* Purpose: 		Junit tests for AppointmentService functionality
* 
*********************************************************/

package tests;

import static org.junit.Assert.assertEquals;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.Test;

import main.Appointment;
import main.AppointmentService;

public class AppointmentServiceTest {

	@Test
	public void TestAddAppointment() throws ParseException {
		
		// Create new date object
		SimpleDateFormat formatter = new SimpleDateFormat("MM-dd-yyyy HH:mm");      
	    Date testDate = formatter.parse("10-20-2021 13:30");
	    
		// Create new appointment service
		AppointmentService apptSvc = new AppointmentService();
	    
	    // Create new appointment
	    Appointment appt = new Appointment(testDate, "Meeting with the Bobs");
	    
	    // Test adding valid appointment to service expecting success
	 	assertEquals(true, apptSvc.addAppointment(appt));	
	    
	}
	
	@Test
	public void TestAddDuplicateAppointment() throws ParseException {
		
		// Create new date object
		SimpleDateFormat formatter = new SimpleDateFormat("MM-dd-yyyy HH:mm");      
	    Date testDate = formatter.parse("10-20-2021 13:30");
			    
	    // Create new appointment service
	 	AppointmentService apptSvc = new AppointmentService();
	 	    
 	    // Create new appointment
 	    Appointment appt1 = new Appointment(testDate, "Meeting with the Bobs");
		
		// Add appointment to service
 	    apptSvc.addAppointment(appt1);
		
		// Create second new appointment with identical parameters
 	   	Appointment appt2 = new Appointment(testDate, "Meeting with the Bobs");
		
		// Test adding second appointment to service expecting failure for duplicate parameters
		assertEquals(false, apptSvc.addAppointment(appt2));	
	}
	
	@Test
	public void TestRemoveKnownAppointment() throws ParseException {
		
		// Create new date object
		SimpleDateFormat formatter = new SimpleDateFormat("MM-dd-yyyy HH:mm");      
	    Date testDate = formatter.parse("10-20-2021 13:30");
			    
	    // Create new appointment service
	 	AppointmentService apptSvc = new AppointmentService();
		
	 	// Create new appointment
 	    Appointment appt = new Appointment(testDate, "Meeting with the Bobs");
		
 	    // Add appointment to service
 	    apptSvc.addAppointment(appt);
		
		// Test removing known appointment from service expecting success
		assertEquals(true, apptSvc.removeAppointment(appt.getID()));		
	}
	
	@Test
	public void TestRemoveUnknownTask() throws ParseException {
		
		// Create new date object
		SimpleDateFormat formatter = new SimpleDateFormat("MM-dd-yyyy HH:mm");      
	    Date testDate1 = formatter.parse("10-20-2021 13:30");
	    Date testDate2 = formatter.parse("10-30-2021 08:30");
			    
	    // Create new appointment service
	 	AppointmentService apptSvc = new AppointmentService();
		
	 	// Create new appointment
 	    Appointment appt1 = new Appointment(testDate1, "Meeting with the Bobs");
		
 	    // Create second new appointment
 	    Appointment appt2 = new Appointment(testDate2, "Submit TPS reports with proper cover page");
		
 	    // Add first appointment to service
 	    apptSvc.addAppointment(appt1);
		
		// Test removing unknown appointment from service expecting failure
		assertEquals(false, apptSvc.removeAppointment(appt2.getID()));		
	}	
}
