/*********************************************************
*
* Class Name: 	AppointmentTest
* Author: 		Jason Gross
* Date: 		10/3/2021
* Purpose: 		Junit tests for Appointment functionality
* 
*********************************************************/

package tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.Test;

import main.Appointment;

public class AppointmentTest {

	@Test
	public void NewAppointmentGoodParams() throws ParseException {		
		
		// Create new date object
		SimpleDateFormat formatter = new SimpleDateFormat("MM-dd-yyyy HH:mm");      
	    Date testDate = formatter.parse("10-20-2021 13:30");
		
		// Create new Appointment with valid parameters
		Appointment a1 = new Appointment(testDate, "Meeting with the Bobs");
		
		// Test that parameters have been successfully set
		assertEquals(testDate, a1.getAppointmentDate());
		assertEquals("Meeting with the Bobs", a1.getAppointmentDescription());		
	}
	
	@Test
	public void NewAppointmentBadParamsPastDate() throws ParseException {

		// Create new date object
		SimpleDateFormat formatter = new SimpleDateFormat("MM-dd-yyyy HH:mm");      
	    Date testDate = formatter.parse("7-20-2021 13:30");
		
		// Test passing appointment with date in the past that throws invalid appointment date exception
		try {
			new Appointment(testDate, "Meeting with the Bobs");
		} catch (IllegalArgumentException thrown) {
			assertTrue(thrown.getMessage().equals("Invalid appointment date"));
		}		
	}
	
	@Test
	public void NewAppointmentBadParamsNullDate() throws ParseException {
		
		// Test passing null appointment date throws invalid appointment date exception
		try {
			new Appointment(null, "Meeting with the Bobs");
		} catch (IllegalArgumentException thrown) {
       		assertTrue(thrown.getMessage().equals("Invalid appointment date"));
		}
	}
	
	@Test
	public void NewAppointmentBadParamsLongDescription() throws ParseException {

		// Create new date object
		SimpleDateFormat formatter = new SimpleDateFormat("MM-dd-yyyy HH:mm");      
	    Date testDate = formatter.parse("10-30-2021 12:30");
		
		// Test passing appointment description longer than 60 characters throws invalid appointment description exception
		try {
			new Appointment(testDate, "Meeting with the Bobs to discuss the improvements we can make to company efficiency!");
		} catch (IllegalArgumentException thrown) {
			assertTrue(thrown.getMessage().equals("Invalid appointment description"));
		}
	}
	
	@Test
	public void NewAppointmentBadParamsNullDescription() throws ParseException {

		// Create new date object
		SimpleDateFormat formatter = new SimpleDateFormat("MM-dd-yyyy HH:mm");      
	    Date testDate = formatter.parse("10-30-2021 12:30");
				
		// Test passing null appointment description throws invalid appointment description exception
		try {
			new Appointment(testDate, null);
		} catch (IllegalArgumentException thrown) {
       		assertTrue(thrown.getMessage().equals("Invalid appointment description"));
		}		
	}
}
