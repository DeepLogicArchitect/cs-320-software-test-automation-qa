/*********************************************************
*
* Class Name: 	TaskTest
* Author: 		Jason Gross
* Date: 		9/26/2021
* Purpose: 		Junit tests for Task functionality
* 
*********************************************************/
package tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import main.*;

public class TaskTest {

	@Test
	public void NewTaskGoodParams() {
		
		// Create new task with valid parameters
		Task t1 = new Task("Groceries", "Weekly shopping list");
		
		// Test that parameters have been successfully set
		assertEquals("Groceries", t1.getTaskName());
		assertEquals("Weekly shopping list", t1.getTaskDescription());		
	}
	
	@Test
	public void NewTaskBadParamsLongName() {
		
		// Test passing task name longer than 20 characters throws invalid task name exception
		try {
			new Task("EndOfTheWorldShoppingExtravaganza", "Some fun stuff to buy before the end");
		} catch (IllegalArgumentException thrown) {
			assertTrue(thrown.getMessage().equals("Invalid task name"));
		}	
	}
	
	@Test
	public void NewTaskBadParamsNullName() {

		// Test passing null task name throws invalid task name exception
		try {
			new Task(null, "Weekly shopping list");
		} catch (IllegalArgumentException thrown) {
       		assertTrue(thrown.getMessage().equals("Invalid task name"));
		}
	}
	
	@Test
	public void NewTaskBadParamsLongDescription() {
				
		// Test passing task description longer than 60 characters throws invalid task description exception
		try {
			new Task("Groceries", "I've got a lovely bunch of coconuts, there they are all standing in a row!");
		} catch (IllegalArgumentException thrown) {
			assertTrue(thrown.getMessage().equals("Invalid task description"));
		}	
	}
	
	@Test
	public void NewTaskBadParamsNullDescription() {
				
		// Test passing null task description throws invalid task description exception
		try {
			new Task("Groceries", null);
		} catch (IllegalArgumentException thrown) {
       		assertTrue(thrown.getMessage().equals("Invalid task description"));
		}		
	}
}
