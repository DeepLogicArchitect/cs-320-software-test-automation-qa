/*********************************************************
*
* Class Name: 	TaskServiceTest
* Author: 		Jason Gross
* Date: 		9/26/2021
* Purpose: 		Junit tests for TaskService functionality
* 
*********************************************************/
package tests;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import main.TaskService;
import main.Task;

public class TaskServiceTest {
	
	@Test
	public void TestAddTask() {
		
		// Create new task service
		TaskService ts = new TaskService();
		
		// Create new task
		Task t1 = new Task("Groceries", "Weekly shopping list");
		
		// Test adding valid task to service expecting success
		assertEquals(true, ts.addTask(t1));		
	}
	
	@Test
	public void TestAddDuplicateTask() {
		
		// Create new task service
		TaskService ts = new TaskService();
		
		// Create new task
		Task t1 = new Task("Groceries", "Weekly shopping list");
		
		// Add task to service
		ts.addTask(t1);
		
		// Create second new task with identical parameters
		Task t2 = new Task("Groceries", "Weekly shopping list");
		
		// Test adding second task to service expecting failure for duplicate parameters
		assertEquals(false, ts.addTask(t2));	
	}
	
	@Test
	public void TestRemoveKnownTask() {
		
		// Create new task service
		TaskService ts = new TaskService();
		
		// Create new task
		Task t1 = new Task("Groceries", "Weekly shopping list");
		
		// Add task to service
		ts.addTask(t1);
		
		// Test removing known task from service expecting success
		assertEquals(true, ts.removeTask(t1.getID()));		
	}
	
	@Test
	public void TestRemoveUnknownTask() {
		
		// Create new task service
		TaskService ts = new TaskService();
		
		// Create new task
		Task t1 = new Task("Groceries", "Weekly shopping list");
		
		// Create another new task
		Task t2 = new Task("Party", "Plan a party worthy of the name");
		
		// Add first task to service
		ts.addTask(t1);
		
		// Test removing unknown task from service expecting failure
		assertEquals(false, ts.removeTask(t2.getID()));		
	}
	
	@Test
	public void TestUpdateGoodTask() {
		
		// Create new task service
		TaskService ts = new TaskService();
		
		// Create new task
		Task t1 = new Task("Groceries", "Weekly shopping list");
		
		// Add task to service
		ts.addTask(t1);
				
		// Test updating task with new parameters expecting success
		assertEquals(true, ts.updateTask(t1.getID(),"Party", "Plan a party worthy of the name"));
		
		// Test that all parameters have truly updated
		assertEquals("Party", t1.getTaskName());
		assertEquals("Plan a party worthy of the name", t1.getTaskDescription());		
	}
	
	@Test
	public void TestUpdateBadTask() {
		
		// Create new task service
		TaskService ts = new TaskService();
		
		// Create new task
		Task t1 = new Task("Groceries", "Weekly shopping list");
		
		// Add task to service
		ts.addTask(t1);
				
		// Test updating task with duplicate parameters expecting failure
		assertEquals(false, ts.updateTask(t1.getID(),"Groceries", "Weekly shopping list"));
		
		// Test updating task with a different errant parameter each time expecting all failures
		assertEquals(false, ts.updateTask(t1.getID(),"EndOfTheWorldShoppingExtravaganza", "Some fun stuff to buy before the end"));	
		assertEquals(false, ts.updateTask(t1.getID(),"Groceries", "I've got a lovely bunch of coconuts, there they are all standing in a row!"));	
		assertEquals(false, ts.updateTask(t1.getID(),null, null));
	}	
}
