/*********************************************************
 * 
 * Class Name: 	Contact
 * Author: 		Jason Gross
 * Date: 		9/19/2021
 * Purpose: 	Object that stores a single record of
 * 				contact information including first and
 * 				last name, phone number, address, and
 * 				a unique identifier.
 *          
 *********************************************************/

package main;

public class Contact {

	// =============================
	// Attributes
	// =============================
	private String id;
	private String firstName;
	private String lastName;
	private String phone;
	private String address;
	
	// =============================
	// Constructor
	// =============================
	
	public Contact(String firstName, String lastName, String phone, String address ) {
		
		// Set contact information
		setFirstName(firstName);
		setLastName(lastName);
		setPhone(phone);
		setAddress(address);
		
		// Assign unique alpha-numeric identifier of 10 characters
		this.id = generateUniqueIdentifier(10);
		
	}
	
	// =============================
	// Accessors
	// =============================
	
	// Returns contact ID
	public String getID() {
		return id;
	}
		
	// Returns first name
	public String getFirstName() {
		return firstName;
	}
	
	// Returns last name
	public String getLastName() {
		return lastName;
	}
	
	// Returns phone number
	public String getPhone() {
		return phone;
	}
	
	// Returns address
	public String getAddress() {
		return address;
	}
	
	// =============================
	// Methods to set attributes
	// =============================
	
	// Sets first name
	public void setFirstName(String firstName) {
		
		// If first name is not valid
		if (!isValidFirstName(firstName)) {
			
			// Throw exception
			throw new IllegalArgumentException("Invalid first name");
		}
		else
		{
			// Set first name
			this.firstName = firstName;
		}
	}
	
	// Sets last name
	public void setLastName(String lastName) {
		
		// If last name is not valid
		if (!isValidLastName(lastName)) {
			
			// Throw exception
			throw new IllegalArgumentException("Invalid last name");
		}
		else
		{
			// Sets last name
			this.lastName = lastName;
		}
	}
	
	// Sets phone number
	public void setPhone(String phone) {
		
		// If phone number is not valid
		if (!isValidPhone(phone)) {
			
			// Throw exception
			throw new IllegalArgumentException("Invalid phone number");
		}
		else
		{
			// Sets phone number
			this.phone = phone;
		}
	}
	
	// Sets address
	public void setAddress(String address) {
		
		// If address is not valid
		if (!isValidAddress(address)) {
			
			// Throw exception
			throw new IllegalArgumentException("Invalid address");
		}
		else
		{
			// Sets address
			this.address = address;
		}
	}
	
	// =============================
	// Validation methods
	// =============================
	
	// Ensures first name meets acceptable criteria
	public boolean isValidFirstName(String testFirstName) {
		
		// If first name is null or greater than 10 characters
		if (testFirstName == null || testFirstName.length() > 10) {
			
			// Return failure
			return false;
		}
		else
		{
			// Return success
			return true;
		}
	}
	
	// Ensures last name meets acceptable criteria
	public boolean isValidLastName(String testLastName) {
		
		// If last name is null or greater than 10 characters
		if (testLastName == null || testLastName.length() > 10) {
			
			// Return failure
			return false;
		}
		else
		{
			// Return success
			return true;
		}
	}
	
	// Ensures phone number meets acceptable criteria
	public boolean isValidPhone(String testPhone) {
		
		// If phone number is not numeric or not exactly 10 characters
		if (!isNumeric(testPhone) || testPhone.length() != 10) {
			
			// Return failure
			return false;
		}
		else
		{
			// Return success
			return true;
		}
	}
	
	// Ensures address meets acceptable criteria
	public boolean isValidAddress(String testAddress) {
		
		// If address is null or greater than 30 characters
		if (testAddress == null || testAddress.length() > 30) {
			
			// Return failure
			return false;
		}
		else
		{
			// Return success
			return true;
		}
	}
	
	// Function to test if a string is numeric
	private boolean isNumeric(String str)
	{
		// Iterate each character in the string
	    for (char c : str.toCharArray())
	    {
	    	// Return false if current character is not a digit
	        if (!Character.isDigit(c)) return false;
	    }
	    
	    // Return true
	    return true;
	}
	
	// =============================
	// Helper methods
	// =============================
	
	// Function to generate a random string of length n
    private String generateUniqueIdentifier(int n)
    {
  
        // Create string of available characters to choose from
        String characterChoices = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvxyz0123456789";
  
        // create StringBuilder the size of the random string we're generating
        StringBuilder sb = new StringBuilder(n);
  
        // Iterate each character of return string
        for (int i = 0; i < n; i++) {
  
            // Select a random position in the base string
            int index = (int)(characterChoices.length() * Math.random());
  
            // Append alpha-numeric character from that position to end of return string
            sb.append(characterChoices.charAt(index));
        }
  
        return sb.toString();
    }
}