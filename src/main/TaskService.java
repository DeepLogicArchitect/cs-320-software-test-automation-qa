/*********************************************************
 * 
 * Class Name: 	TaskService
 * Author: 		Jason Gross
 * Date: 		9/26/2021
 * Purpose: 	Maintains an array list of task objects
 *          	with methods to add, remove and update 
 *          	tasks.
 *          
 *********************************************************/

package main;

import java.util.ArrayList;

public class TaskService {

	// Holds an array of tasks
	private ArrayList<Task> taskList = new ArrayList<Task>();
	
	// Adds a new task to the array
	public boolean addTask(Task newTask) {

		// If new task is a duplicate
		if (isDuplicate(newTask.getTaskName(), newTask.getTaskDescription())) {
		
			// Display debug message
    		System.out.println("Duplicate parameters - Could not add task: " + newTask.getID() + ".");
		}
		else {
			
			try {
				
				// Add task to array list
				taskList.add(newTask);
				
	        } catch (Exception e) {
	        	
	        	// Display debug message
	    		//System.out.println("Could not add task - " + e.getMessage());
	    		
	        	// Return failure
	        	return false;
	        }
			
			// Return success
			return true;			
		}
		
		// Return failure
		return false;			
	}
	
	// Removes task from array that has specified task ID
	public boolean removeTask(String taskID) {
		
		// Iterate through each task in task array list
		for (Task searchTask : taskList) {
			
			// If current task ID is the one we are searching for
			if (searchTask.getID().equals(taskID)) {
				
				try {
					
					// Remove task
					taskList.remove(searchTask);
					
		        } catch (Exception e) {
		        	
		        	// Display debug message
		    		//System.out.println("Could not remove task - " + e.getMessage());
		    		
		        	// Return failure
		        	return false;
		        }
				
				// Return success
				return true;
			}
		}
		
		// Display debug message
		System.out.println("Could not find task " + taskID + " to remove.");
		
		// Return failure
		return false;
	}
	
	// Updates task in array that has specified task ID
	public boolean updateTask(String taskID, String taskName, String taskDescription ) {
		
		// Iterate through each task in  array list
		for (Task searchTask : taskList) {
			
			// If current task ID is the one we are searching for
			if (searchTask.getID().equals(taskID)) {
				
				// Check valid parameters
				if (searchTask.isValidTaskName(taskName) &&
						searchTask.isValidTaskDescription(taskDescription)) {
				
					// Check for duplicate task
					if (isDuplicate(taskName, taskDescription)) {
						
						// Display debug message
			    		System.out.println("Duplicate parameters - Could not update task: " + taskID + ".");
					}
					else {
						// Update task information
						searchTask.setTaskName(taskName);
						searchTask.setTaskDescription(taskDescription);

						// Return success
						return true;
					}
				}
				else {
					
					// Display debug message
		    		System.out.println("Invalid parameters - Could not update task: " + taskID + ".");
				}
			}
		}
		
		// Return failure
		return false;
	}
	
	// Checks array for a task with the same task name, and task description
	public boolean isDuplicate(String searchTaskName, String searchTaskDescription) {
		
		// Iterate through each task in task array list
		for (Task searchTask : taskList) {
			
			// If all parameters match the existing task
			if (  searchTask.getTaskName().equals(searchTaskName) &&
					searchTask.getTaskDescription().equals(searchTaskDescription) ) {
				
				// Return success
				return true;
			}			
		}
			
		// Return failure
		return false;		
	}
}
