/*********************************************************
 * 
 * Class Name: 	Appointment
 * Author: 		Jason Gross
 * Date: 		10/3/2021
 * Purpose: 	Object that stores a single record of
 * 				appointment information including date, description,
 * 				and a unique identifier.
 *          
 *********************************************************/

package main;

import java.util.Date;

public class Appointment {

	// =============================
	// Attributes
	// =============================
	private String id;
	private Date appointmentDate;
	private String appointmentDescription;
	
	// =============================
	// Constructor
	// =============================
	
	public Appointment(Date appointmentDate, String appointmentDescription ) {
		
		// Set appointment information
		setAppointmentDate(appointmentDate);
		setAppointmentDescription(appointmentDescription);
		
		// Assign unique alpha-numeric identifier of 10 characters
		this.id = generateUniqueIdentifier(10);		
	}
	
	// =============================
	// Accessors
	// =============================
	
	// Returns appointment ID
	public String getID() {
		return id;
	}
		
	// Returns appointment name
	public Date getAppointmentDate() {
		return appointmentDate;
	}
	
	// Returns appointment description
	public String getAppointmentDescription() {
		return appointmentDescription;
	}	
	
	// =============================
	// Methods to set attributes
	// =============================
	
	// Sets appointment date
	public void setAppointmentDate(Date appointmentDate) {
		
		// If appointment date is not valid
		if (!isValidAppointmentDate(appointmentDate)) {
			
			// Throw exception
			throw new IllegalArgumentException("Invalid appointment date");
		}
		else
		{
			// Set appointment date
			this.appointmentDate = appointmentDate;
		}
	}
	
	// Sets appointment description
	public void setAppointmentDescription(String appointmentDescription) {
		
		// If appointment description is not valid
		if (!isValidAppointmentDescription(appointmentDescription)) {
			
			// Throw exception
			throw new IllegalArgumentException("Invalid appointment description");
		}
		else
		{
			// Sets appointment description
			this.appointmentDescription = appointmentDescription;
		}
	}
			
	// =============================
	// Validation methods
	// =============================
	
	// Ensures appointment date meets acceptable criteria
	public boolean isValidAppointmentDate(Date testAppointmentDate) {
		
		// If appointment date is null or in the past
		if (testAppointmentDate == null || testAppointmentDate.before(new Date())) {
			
			// Return failure
			return false;
		}
		else
		{
			// Return success
			return true;
		}
	}
	
	public boolean before(Date testAppointmentDate) {
		
		return false;
	}

	// Ensures appointment description meets acceptable criteria
	public boolean isValidAppointmentDescription(String testAppointmentDescription) {
		
		// If appointment description is null or greater than 50 characters
		if (testAppointmentDescription == null || testAppointmentDescription.length() > 50) {
			
			// Return failure
			return false;
		}
		else
		{
			// Return success
			return true;
		}
	}
		
	// =============================
	// Helper methods
	// =============================
	
	// Function to generate a random string of length n
    private String generateUniqueIdentifier(int n)
    {
  
        // Create string of available characters to choose from
        String characterChoices = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvxyz0123456789";
  
        // create StringBuilder the size of the random string we're generating
        StringBuilder sb = new StringBuilder(n);
  
        // Iterate each character of return string
        for (int i = 0; i < n; i++) {
  
            // Select a random position in the base string
            int index = (int)(characterChoices.length() * Math.random());
  
            // Append alpha-numeric character from that position to end of return string
            sb.append(characterChoices.charAt(index));
        }
  
        return sb.toString();
    }
}
