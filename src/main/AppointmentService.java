/*********************************************************
 * 
 * Class Name: 	AppointmentService
 * Author: 		Jason Gross
 * Date: 		10/3/2021
 * Purpose: 	Maintains an array list of appointment objects
 *          	with methods to add and remove appointments.
 *          
 *********************************************************/

package main;

import java.util.ArrayList;
import java.util.Date;

public class AppointmentService {
	
	// Holds an array of appointments
	private ArrayList<Appointment> appointmentList = new ArrayList<Appointment>();
	
	// Adds a new appointment to the array
	public boolean addAppointment(Appointment newAppointment) {

		// If new appointment is a duplicate
		if (isDuplicate(newAppointment.getAppointmentDate(), newAppointment.getAppointmentDescription())) {
		
			// Display debug message
    		System.out.println("Duplicate parameters - Could not add appointment: " + newAppointment.getID() + ".");
		}
		else {
			
			try {
				
				// Add appointment to array list
				appointmentList.add(newAppointment);
				
	        } catch (Exception e) {
	        	
	        	// Display debug message
	    		//System.out.println("Could not add appointment - " + e.getMessage());
	    		
	        	// Return failure
	        	return false;
	        }
			
			// Return success
			return true;			
		}
		
		// Return failure
		return false;			
	}
	
	// Removes appointment from array that has specified appointment ID
	public boolean removeAppointment(String appointmentID) {
		
		// Iterate through each appointment in appointment array list
		for (Appointment searchAppointment : appointmentList) {
			
			// If current appointment ID is the one we are searching for
			if (searchAppointment.getID().equals(appointmentID)) {
				
				try {
					
					// Remove appointment
					appointmentList.remove(searchAppointment);
					
		        } catch (Exception e) {
		        	
		        	// Display debug message
		    		//System.out.println("Could not remove appointment - " + e.getMessage());
		    		
		        	// Return failure
		        	return false;
		        }
				
				// Return success
				return true;
			}
		}
		
		// Display debug message
		//System.out.println("Could not find appointment " + appointmentID + " to remove.");
		
		// Return failure
		return false;
	}
	
	// Checks array for a appointment with the same appointment date, and appointment description
	public boolean isDuplicate(Date searchAppointmentDate, String searchAppointmentDescription) {
		
		// Iterate through each appointment in appointment array list
		for (Appointment searchAppointment : appointmentList) {
			
			// If all parameters match the existing appointment
			if (  searchAppointment.getAppointmentDate().equals(searchAppointmentDate) &&
					searchAppointment.getAppointmentDescription().equals(searchAppointmentDescription) ) {
				
				// Return success
				return true;
			}			
		}
			
		// Return failure
		return false;		
	}	
}
