/*********************************************************
 * 
 * Class Name: 	ContactService
 * Author: 		Jason Gross
 * Date: 		9/19/2021
 * Purpose: 	Maintains an array list of contact objects
 *          	with methods to add, remove and update 
 *          	contacts.
 *          
 *********************************************************/

package main;

import java.util.ArrayList;

public class ContactService {

	// Holds an array of contacts
	private ArrayList<Contact> contactList = new ArrayList<Contact>();
	
	// Adds a new contact to the array
	public boolean addContact(Contact newContact) {

		// If the new contact object we're trying to add has all duplicate parameters
		if (isDuplicate(newContact.getFirstName(), newContact.getLastName(), newContact.getPhone(), newContact.getAddress())) {
		
			// Display debug message
    		System.out.println("Duplicate parameters - Could not add contact: " + newContact.getID() + ".");
		}
		else {
			
			try {
				
				// Add contact to array list
				contactList.add(newContact);
				
	        } catch (Exception e) {
	        	
	        	// Display debug message
	    		//System.out.println("Could not add contact - " + e.getMessage());
	    		
	        	// Return failure
	        	return false;
	        }
			
			// Return success
			return true;			
		}
		
		// Return failure
		return false;			
	}
	
	// Removes contact from array that has specified contact ID
	public boolean removeContact(String contactID) {
		
		// Iterate through each contact in contact array list
		for (Contact searchContact : contactList) {
			
			// If current contact ID is the one we are searching for
			if (searchContact.getID().equals(contactID)) {
				
				try {
					
					// Remove contact
					contactList.remove(searchContact);
					
		        } catch (Exception e) {
		        	
		        	// Display debug message
		    		//System.out.println("Could not remove contact - " + e.getMessage());
		    		
		        	// Return failure
		        	return false;
		        }
				
				// Return success
				return true;
			}
		}
		
		// Display debug message
		//System.out.println("Could not find contact " + contactID + " to remove.");
		
		// Return failure
		return false;
	}
	
	// Updates contact in array that has specified contact ID
	public boolean updateContact(String contactID, String firstName, String lastName, String phone, String address ) {
		
		// Iterate through each contact in contact array list
		for (Contact searchContact : contactList) {
			
			// If current contact ID is the one we are searching for
			if (searchContact.getID().equals(contactID)) {
				
				// Check valid parameters
				if (searchContact.isValidFirstName(firstName) &&
					searchContact.isValidLastName(lastName) &&
					searchContact.isValidPhone(phone) &&
					searchContact.isValidAddress(address)) {
				
					// Check for duplicate contact
					if (isDuplicate(firstName, lastName, phone, address)) {
						
						// Display debug message
			    		System.out.println("Duplicate parameters - Could not update contact: " + contactID + ".");
					}
					else {
						// Update contact information
						searchContact.setFirstName(firstName);
						searchContact.setLastName(lastName);
						searchContact.setPhone(phone);
						searchContact.setAddress(address);
						
						// Return success
						return true;
					}
				}
				else {
					
					// Display debug message
		    		System.out.println("Invalid parameters - Could not update contact: " + contactID + ".");
				}
			}
		}
		
		// Return failure
		return false;
	}
	
	// Checks array for a contact with the same first name, last name, address, and phone number
	public boolean isDuplicate(String searchFirstName, String searchLastName, String searchPhone, String searchAddress) {
		
		// Iterate through each contact in contact array list
		for (Contact searchContact : contactList) {
			
			// If all parameters match the existing contact
			if (  searchContact.getFirstName().equals(searchFirstName) &&
				  searchContact.getLastName().equals(searchLastName) && 
				  searchContact.getPhone().equals(searchPhone) && 
				  searchContact.getAddress().equals(searchAddress) ) {
				
				// Return success
				return true;
			}			
		}
			
		// Return failure
		return false;		
	}
}
