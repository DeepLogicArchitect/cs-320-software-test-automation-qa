/*********************************************************
 * 
 * Class Name: 	Task
 * Author: 		Jason Gross
 * Date: 		9/26/2021
 * Purpose: 	Object that stores a single record of
 * 				task information including name, description,
 * 				and a unique identifier.
 *          
 *********************************************************/

package main;

public class Task {

	// =============================
	// Attributes
	// =============================
	private String id;
	private String taskName;
	private String taskDescription;
	
	// =============================
	// Constructor
	// =============================
	
	public Task(String taskName, String taskDescription ) {
		
		// Set task information
		setTaskName(taskName);
		setTaskDescription(taskDescription);
		
		// Assign unique alpha-numeric identifier of 10 characters
		this.id = generateUniqueIdentifier(10);
		
	}
	
	// =============================
	// Accessors
	// =============================
	
	// Returns task ID
	public String getID() {
		return id;
	}
		
	// Returns task name
	public String getTaskName() {
		return taskName;
	}
	
	// Returns task description
	public String getTaskDescription() {
		return taskDescription;
	}	
	
	// =============================
	// Methods to set attributes
	// =============================
	
	// Sets first name
	public void setTaskName(String taskName) {
		
		// If task name is not valid
		if (!isValidTaskName(taskName)) {
			
			// Throw exception
			throw new IllegalArgumentException("Invalid task name");
		}
		else
		{
			// Set task name
			this.taskName = taskName;
		}
	}
	
	// Sets task description
	public void setTaskDescription(String taskDescription) {
		
		// If task description is not valid
		if (!isValidTaskDescription(taskDescription)) {
			
			// Throw exception
			throw new IllegalArgumentException("Invalid task description");
		}
		else
		{
			// Sets task description
			this.taskDescription = taskDescription;
		}
	}
			
	// =============================
	// Validation methods
	// =============================
	
	// Ensures task name meets acceptable criteria
	public boolean isValidTaskName(String testTaskName) {
		
		// If task name is null or greater than 20 characters
		if (testTaskName == null || testTaskName.length() > 20) {
			
			// Return failure
			return false;
		}
		else
		{
			// Return success
			return true;
		}
	}
	
	// Ensures task description meets acceptable criteria
	public boolean isValidTaskDescription(String testTaskDescription) {
		
		// If task description is null or greater than 50 characters
		if (testTaskDescription == null || testTaskDescription.length() > 50) {
			
			// Return failure
			return false;
		}
		else
		{
			// Return success
			return true;
		}
	}
		
	// =============================
	// Helper methods
	// =============================
	
	// Function to generate a random string of length n
    private String generateUniqueIdentifier(int n)
    {
  
        // Create string of available characters to choose from
        String characterChoices = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvxyz0123456789";
  
        // create StringBuilder the size of the random string we're generating
        StringBuilder sb = new StringBuilder(n);
  
        // Iterate each character of return string
        for (int i = 0; i < n; i++) {
  
            // Select a random position in the base string
            int index = (int)(characterChoices.length() * Math.random());
  
            // Append alpha-numeric character from that position to end of return string
            sb.append(characterChoices.charAt(index));
        }
  
        return sb.toString();
    }
}